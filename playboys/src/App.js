import React from 'react';

import './App.css';
import playboys from  './assets/files/plays.docx'
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>이거 회칙이야 애들아</h1>
        <div>
          다운은 여기서 
          <a href={playboys} download>다운받으삼</a>
        </div>
        <div className="text">
        {`플레이보이즈회 회칙


제1장 총칙
제1조(목적) 플레이보이즈회(이하 ‘플보회’라고 한다)는 회원 간의 친목의 유지와 발전을 위해 조직하였다.

제2장 회원
제2조(회원) 플보회의 회원은 김종빈, 김환웅, 송해준, 원영종, 이상무, 이수한, 임재우, 장재완, 전영호, 한태은, 홍현승으로 한다.

제3조(회원의 가입) 새로운 회원의 가입은 회원 만장일치의 동의를 얻어서 한다.

제4조(회원의 탈퇴) ① 회원은 임의로 회에서 탈퇴할 수 있다.
② 회원이 사망한 경우 회를 자동 탈퇴한다.

제5조(회원의 제명) ① 회원이 중범죄를 범하거나 다른 회원을 상대로 사기 등의 범죄를 범하는 등의 이유로 같이 플보회에 소속되기 어려울 정도로 다른 회원들로부터 신뢰를 잃어버린 경우에 회원을 제명할 수 있다.
② 전 항의 제명은 회장의 발의와 회원 만장일치의 동의로써 한다.

제3장 임원
제6조(회장) ① 플보회를 대표하기 위해 회장을 둔다.
② 회장은 임재우로 한다.
③ 회장은 회원 만장일치의 동의로써 교체한다.
④ 회장의 사망, 탈퇴, 제명 혹은 사임 등으로 회장직이 공석이 된 경우 회원 삼분의이 이상의 동의로 새로운 회장을 선출한다.

제7조(총무) ① 회비의 납부, 사용, 반환 등을 관리하기 위하여 총무를 둔다.
② 총무는 전영호로 한다.
③ 총무는 회장의 발의와 회원 삼분의이 이상의 동의로써 교체한다.
④ 총무의 사망, 탈퇴, 제명 혹은 사임 등으로 총무직이 공석이 된 경우 회원 삼분의이 이상의 동의로 새로운 총무를 선출한다.

제4장 회비
제8조(회비의 목적) 회비는 회원의 친목을 위한 목적으로 사용하기 위하여 모은다.

제9조(회비의 납부) ① 회비는 월 2만원으로 한다.
② 회비는 총무의 계좌로 매월 20일에 납부한다.
③ 회비의 액수 및 납부일자는 회원 삼분의 이 이상의 동의로써 변경한다.

제10조(회비 납부의 연기) 회원은 회장 혹은 총무의 동의를 받아 회비 납부를 연기할 수 있다. 단, 이 경우에도 6월을 넘는 회비를 연기하지는 못한다.

제11조(회비 납부의 연체) 3월 이상의 회비를 연체한 경우 총무는 연체 회비의 십분의일에 해당하는 연체료를 부과할 수 있다.

제12조(회비의 관리) ① 총무는 회비를 올바른 방법으로 관리한다.
② 회비의 보존행위는 총무 재량으로 할 수 있다.
③ 회비를 이용한 수익행위는 회원 삼분의이 이상의 동의로 할 수 있다.

제13조(회비의 사용) ① 회비는 회원 삼분의이 이상의 동의하에 다음과 같은 경우에 사용한다: 경조사비, 여행자금, 회식비
② 회비를 사용한 경우 회비를 사용하는 행사 등에 참여하지 못하는 회원에게 당시 지출한 회비를 회원의 수로 나눈 금액의 삼분의이를 지급한다.
③ 전항의 사유로 지급할 회비가 부족한 경우 부족한 금액만큼 회비를 반환받아야 하는 회원이 앞으로 납부할 회비를 면제한다.

제14조(회비의 반환) ① 플보회에 가입중인 회원은 회비의 반환을 요구할 수 없다.
② 제4조 제2항의 사유로 회원이 플보회를 탈퇴한 경우 현존 회비를 회원 수로 나눈 금액만큼을 회원의 상속인에게 반환한다.
③ 제5조의 사유로 회원이 플보회에서 제명당한 경우 현존 회비를 회원 수로 나눈 금액만큼을 제명당한 회원에게 반환한다. 
④ 제4조 제1항의 사유로 회원이 플보회를 탈퇴한 경우 회비를 반환하지 않는다.
⑤ 제15조 제2항의 사유로 회가 소멸한 경우 현존 회비를 회원 수로 나눈 금액만큼을 회원이었던 자들에게 균등히 반환한다.

제5장 플보회의 소멸
제15조(회의 소멸) ① 플보회는 회원이 한 명도 남아 있지 않은 경우 소멸한다.
② 회를 더 이상 유지할 이유가 없다고 생각되는 경우 회원 모두의 동의로써 회를 소멸한다.

제6장 회칙의 개정
제16조(개정) 회칙은 회장의 주재로 회원 삼분의이 이상이 모인 자리에서 참석 회원 삼분의이 이상의 동의로써 개정한다.

2019.08.08


`}
        </div>
     


      </header>
    </div>
  );
}

export default App;
